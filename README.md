# Paint3D - Clone

## Description
This is a training project to train on Using C++, VTK and Qt5 to make a basic user inteface that deals with basic graphics objects.

## Features
- You can select an object to draw, it can be a Cube, Sphere or Cylinder.
- You can select from three colors: Red, Green and Blue.
- When you click the mouse on the rendering window it will draw the selected shape with the selected color.
  * the shape dimension is around 0.01 unit.
  * it will be drawn one unit away from the camera at the selected point (one unit away in the direction of camera projection vector).
- You get all the nice features of QVTKOpenGLNativeWidget such as zooming and camera rotation.

## Disclaimer
- The initial target of the program was training.
- with limited time comes limited functionalities.
- The initial intended features was more than that, but I am simply tired.
- I am happy that I did some graphics processing.
- This isn't probably useful as project but it is a good example for beginners.
- I installed the requirements on a Windows 10 VM, the VM started to act strangly and occupied more than 100 GB and that was a wast of two days, linux is better.
- This project was done on Ubuntu 20.04 using CMake, QtCreator, g++ v9.3, Qt v5.12.8 and VTK v9.0.3.
