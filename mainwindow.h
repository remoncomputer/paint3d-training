#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// C++ Includes
#include <string>

// VTK includes
#include <vtkSmartPointer.h>
//#include <vtkNew.h>
#include <vtkSmartPointer.h>
#include <vtkNamedColors.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkPolyDataAlgorithm.h>

// Qt includes
#include <QMainWindow>

using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow();

private:
    Ui::MainWindow *ui;
    //The color palette
    vtkNew<vtkNamedColors> colors;
    // The Render window for display
    vtkNew<vtkGenericOpenGLRenderWindow> renderWindow;
    // VTK Renderer
    vtkNew<vtkRenderer> renderer;
    // The selected color
    string selectedColorName;
    // The selected object act
    int drag_start_x, drag_start_y;
    enum SourceType {Cube, Sphere, Cylinder};
    // The selected source shape
    SourceType selectedSourceType;

    //template<typename sourceT>
    void addShape(vtkSmartPointer<vtkPolyDataAlgorithm> source);
    //Left mouse clicked on render window event
    static void leftMousePressOnRenderWindowCallback(vtkObject *caller, unsigned long eid, void *clientdata, void *calldata);




public slots:
    void slotExit();
private slots:
    void on_selectBlueBtn_clicked();
    void on_selectGreenBtn_clicked();
    void on_selectRedBtn_clicked();
    void on_drawSphereBtn_clicked();
    void on_drawCubeBtn_clicked();
    void on_drawCylinderBtn_clicked();
};
#endif // MAINWINDOW_H
