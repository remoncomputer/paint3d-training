#include "mainwindow.h"
#include "./ui_mainwindow.h"

// VTK includes
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include <vtkVersion.h>
#include <vtkCallbackCommand.h>
#include <vtkNew.h>
#include <vtkCoordinate.h>
#include <vtkCamera.h>
#include <vtkCubeSource.h>
#include <vtkCylinderSource.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->label->setText("Constructor called");
    this->selectedSourceType = MainWindow::SourceType::Cube;

    //initialization of UI state variables
    selectedColorName = "Tomato";

    // Setting the handler for the render window widget
    this->ui->renderWindow->setRenderWindow(renderWindow);

    //Adding the sphere
    // Sphere
    //vtkNew<vtkSphereSource> sphereSource;
    //vtkNew<vtkPolyDataAlgorithm> s1 = sphereSource;

//    sphereSource->Update();
//    vtkNew<vtkPolyDataMapper> sphereMapper;
//    sphereMapper->SetInputConnection(sphereSource->GetOutputPort());
//    vtkNew<vtkActor> sphereActor;
//    sphereActor->SetMapper(sphereMapper);
//    sphereActor->GetProperty()->SetColor(colors->GetColor4d("Tomato").GetData());
//    renderer->AddActor(sphereActor);
    //addShape(sphereSource);

    renderer->SetBackground(colors->GetColor3d("SteelBlue").GetData());

    // Adding the render to the window
    //this->ui->renderWindow->GetRenderWindow()->AddRenderer(renderer);
    renderWindow->AddRenderer(renderer);
    renderWindow->SetWindowName("Paint3D");

    //Setting the mouse events
    vtkSmartPointer<vtkCallbackCommand> mouseLeftPressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
    mouseLeftPressCallback->SetCallback(
//        [&](vtkObject *caller, unsigned long eid, void *clientdata, void *calldata){
//        this->leftMousePressOnRenderWindowCallback(caller, eid, clientdata, calldata);}

    leftMousePressOnRenderWindowCallback);
    mouseLeftPressCallback->SetClientData(this);
    //this->ui->renderWindow->AddObserver(vtkCallbackCommand::EnterEvent, mouseLeftPressCallback, 1000.0);
    this->ui->renderWindow->interactor()->AddObserver(vtkCallbackCommand::LeftButtonPressEvent, mouseLeftPressCallback);
    //this->ui->renderWindow->
    //renderWindow->AddObserver()
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotExit(){
    qApp->exit();
}

//template <typename sourceT>
void MainWindow::addShape(vtkSmartPointer<vtkPolyDataAlgorithm> source){
    source->Update();
    vtkNew<vtkPolyDataMapper> mapper;
    mapper->SetInputConnection(source->GetOutputPort());
    vtkNew<vtkActor> actor;
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(colors->GetColor4d(this->selectedColorName).GetData());
    renderer->AddActor(actor);
}

void MainWindow::leftMousePressOnRenderWindowCallback(vtkObject *caller, unsigned long eid, void *clientdata, void *calldata){
    MainWindow* mainWindow = static_cast<MainWindow*>(clientdata);
    QVTKInteractor* interactor = dynamic_cast<QVTKInteractor*>(caller);
//    bool isNull = (calldata == nullptr);
//    QMouseEvent* eventData = static_cast<QMouseEvent*>(calldata);
    interactor->GetEventPosition(mainWindow->drag_start_x, mainWindow->drag_start_y);
    mainWindow->ui->label->setText("Event");
//    vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
//    vtkSmartPointer<vtkCubeSource> cubeSource = vtkSmartPointer<vtkCubeSource>::New();
//    vtkSmartPointer<vtkCylinderSource> cylinderSource = vtkSmartPointer<vtkCylinderSource>::New();
    //cubeSource->SetCenter()
    //cylinderSource->setC
//    mainWindow->selectedSource->Set
//    mainWindow->selectedSource = sphereSource;
    //sphereSource->SetCenter(mainWindow->drag_start_x, mainWindow->drag_start_y, 10);
    //double i, i1, i2, i3;
    //mainWindow->renderWindow->Get

    // Drawing the object one uniy away in the direction of the camera projection
    vtkNew<vtkCoordinate> coordinate;
    coordinate->SetCoordinateSystemToDisplay();
    coordinate->SetValue(mainWindow->drag_start_x, mainWindow->drag_start_y, 0);
    double* wordCoordinate = coordinate->GetComputedWorldValue(mainWindow->renderer);
    double* viewPlaneNormalCoordiantes = mainWindow->renderer->GetActiveCamera()->GetDirectionOfProjection();
    double centerX = wordCoordinate[0] + viewPlaneNormalCoordiantes[0];
    double centerY = wordCoordinate[1] + viewPlaneNormalCoordiantes[1];
    double centerZ = wordCoordinate[2] + viewPlaneNormalCoordiantes[2];

    //The shape size
    double shapeSize = 0.01;
    switch(mainWindow->selectedSourceType){
        case MainWindow::SourceType::Cube:
        {
            vtkNew<vtkCubeSource> src;
            src->SetCenter(centerX, centerY, centerZ);
            src->SetBounds(-shapeSize/2, shapeSize/2, -shapeSize/2, shapeSize/2, -shapeSize/2, shapeSize/2);
            mainWindow->addShape(src);
            break;
        }
    case MainWindow::SourceType::Sphere:
        {
            vtkNew<vtkSphereSource> src;
            src->SetCenter(centerX, centerY, centerZ);
            src->SetRadius(shapeSize);
            mainWindow->addShape(src);
            break;
        }
    case MainWindow::SourceType::Cylinder:
        {
            vtkNew<vtkCylinderSource> src;
            src->SetCenter(centerX, centerY, centerZ);
            src->SetRadius(shapeSize);
            src->SetHeight(5 * shapeSize);
            mainWindow->addShape(src);
            break;
        }

    }

//    sphereSource->SetCenter();
//    sphereSource->SetRadius(0.01);
//    mainWindow->addShape(sphereSource);
}

void MainWindow::on_selectBlueBtn_clicked()
{
    this->selectedColorName = "Blue";
}


void MainWindow::on_selectGreenBtn_clicked()
{
    this->selectedColorName = "Green";
}


void MainWindow::on_selectRedBtn_clicked()
{
    this->selectedColorName = "Red";
}


void MainWindow::on_drawSphereBtn_clicked()
{
    this->selectedSourceType = this->Sphere;
}


void MainWindow::on_drawCubeBtn_clicked()
{
    this->selectedSourceType = this->Cube;
}


void MainWindow::on_drawCylinderBtn_clicked()
{
    this->selectedSourceType = this->Cylinder;
}

